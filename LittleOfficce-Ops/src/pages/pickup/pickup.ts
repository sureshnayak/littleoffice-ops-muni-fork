import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { SitesSelectionPage } from '../sites-selection/sites-selection';
import { Observable } from 'rxjs/Observable';
/**
 * Generated class for the PickupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pickup',
  templateUrl: 'pickup.html',
})
export class PickupPage {
  timerVar: any;
  timerVal: any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public alertCtrl: AlertController) {
    this.startTimer();
  }
  startTimer(){
    this.timerVar=Observable.interval(1000).subscribe(x =>{
      this.timerVal = x;
    });
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad PickupPage');
  }
  TakePic(){

  }
  onclickchange(){

  }
  onClickTask(){
    this.navCtrl.push(SitesSelectionPage);
  }
  PresentAlert(){
    let addAlert=this.alertCtrl.create({
      title:"Alert",
      cssClass:"alert-fnt",
      message:"Do You want Discard This Task",
      buttons:[{
          text:"No"
      },
    {
      text:"Yes",
      handler:()=>{
        this.navCtrl.push(SitesSelectionPage);
      }
    }]
    });
    addAlert.present()
  }
}
