import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SitesSelectionPage } from '../sites-selection/sites-selection';

/**
 * Generated class for the AddtaskPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-addtask',
  templateUrl: 'addtask.html',
})
export class AddtaskPage {
  myDate: any;
  tasklist: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddtaskPage');
  }
  onclickchange(){
    this.tasklist=1;
  }
  onClickSave(){
    this.navCtrl.push(SitesSelectionPage);
  }
  onClickCancel(){
    this.navCtrl.pop();
  }
}
