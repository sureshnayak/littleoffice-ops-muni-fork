import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ManPowerSearchPage } from './man-power-search';

@NgModule({
  declarations: [
    ManPowerSearchPage,
  ],
  imports: [
    IonicPageModule.forChild(ManPowerSearchPage),
  ],
})
export class ManPowerSearchPageModule {}
