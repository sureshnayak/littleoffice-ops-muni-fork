import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, AlertController } from 'ionic-angular';
import { Subscription } from 'rxjs';
import { Platform } from 'ionic-angular/platform/platform';
import { Geolocation} from '@ionic-native/geolocation'
import { Storage} from '@ionic/storage'
import {filter} from 'rxjs/operators';
/**
 * Generated class for the FindMyTeamPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
  declare var google;

@IonicPage()
@Component({
  selector: 'page-find-my-team',
  templateUrl: 'find-my-team.html',
})
export class FindMyTeamPage {
  @ViewChild('map') mapElement: ElementRef;
  map: any;
  currentMapTrack = null;


  isTracking = false;
  trakedRoute = [];
  previousTracks = [];

  positionSubscription: Subscription;
  constructor(public navCtrl: NavController,private plt:Platform,private geoLocation:Geolocation,private storage:Storage,private alrtCtrl:AlertController) {
  }

  ionViewDidLoad() {
    this.plt.ready().then(()=>{
      this.loadHistoricRoutes();
      let mapOptions={
        zoom: 13,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: false,
        streetViewControl: false,
        fullScreenControl: false
      };

      this.map=new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    });
    this.geoLocation.getCurrentPosition().then(pos =>{
      let latlng = new google.maps.LatLng(pos.coords.latitude,pos.coords.longitude);
      this.map.setCenter(latlng);
      this.map.setZoom(15);
    }).catch(err=>console.log(err));
  }
  loadHistoricRoutes(){
    this.storage.get('routes').then(data=>{
      if(data){
        this.previousTracks=data;
      }
    });
  }
  startTracking(){
    this.isTracking = true;
    this.trakedRoute= [];
    this.positionSubscription = this.geoLocation.watchPosition()
    .pipe(
      filter(p=>p.coords !== undefined)
    )
    .subscribe(data=>{
      setTimeout(()=>{
        this.trakedRoute.push({lat: data.coords.latitude,lng:data.coords.longitude});
        this.redRawPath(this.trakedRoute);
      })
    })
  }
  redRawPath(path){
    if(this.currentMapTrack){
       this.currentMapTrack.setMap(null);
    }

    if(path.length > 1){
      this.currentMapTrack=new google.maps.Polyline({
        path:path,
        cssClass:"dot-fnt",
        geodesic: true,
        strokeColor: 'blue',
        strokeOpacity:4.0,
        strokeweight:3
      });
      this.currentMapTrack.setMap(this.map);
    }
  }
  stopTracking(){
    let newRoute = {finished: new Date().getTime(), path: this.trakedRoute};
    this.previousTracks.push(newRoute);
    this.storage.set('routes',this.previousTracks);

    this.isTracking = false;
    this.positionSubscription.unsubscribe();
    this.currentMapTrack.setMap(null);
  }
  showHistoryRuote(route){
    this.redRawPath(route);
  }
}
