import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FindMyTeamPage } from './find-my-team';

@NgModule({
  declarations: [
    FindMyTeamPage,
  ],
  imports: [
    IonicPageModule.forChild(FindMyTeamPage),
  ],
})
export class FindMyTeamPageModule {}
