import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddZonePage } from './add-zone';

@NgModule({
  declarations: [
    AddZonePage,
  ],
  imports: [
    IonicPageModule.forChild(AddZonePage),
  ],
})
export class AddZonePageModule {}
