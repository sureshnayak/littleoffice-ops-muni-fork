import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { IonicStorageModule } from '@ionic/storage';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HttpModule } from '@angular/http';
import { Camera } from '@ionic-native/camera';
import { MomentModule } from 'angular2-moment';
import { IonicImageViewerModule } from 'ionic-img-viewer';
import { SitesSelectionPage } from '../pages/sites-selection/sites-selection';
import { AddtaskPage } from '../pages/addtask/addtask';
import { AuditPage } from '../pages/audit/audit';
import { TrainingPage } from '../pages/training/training';
import { DeploymentPage } from '../pages/deployment/deployment';
import { MeetingPage } from '../pages/meeting/meeting';
import { GeneralPage } from '../pages/general/general';
import { ManPowerSearchPage } from '../pages/man-power-search/man-power-search';
import { PurchagePage } from '../pages/purchage/purchage';
import { OthersPage } from '../pages/others/others';
import { HistoryPage } from '../pages/history/history';
import { PickupPage } from '../pages/pickup/pickup';
import { MbscModule } from '@mobiscroll/angular-lite';
import { DeployPopoverComponent } from '../components/deploy-popover/deploy-popover';
import { SignupPage } from '../pages/signup/signup';
import { HomeScreenPage } from '../pages/home-screen/home-screen';
import { ForgotPage } from '../pages/forgot/forgot';
import { PracticePage } from '../pages/practice/practice';
import { MediaCapture } from '@ionic-native/media-capture';
import { Media} from '@ionic-native/media';
import { File } from '@ionic-native/file';
import { ScheduleManagementPage } from '../pages/schedule-management/schedule-management';
import { SetUpPage } from '../pages/set-up/set-up';
import { FindMyTeamPage } from '../pages/find-my-team/find-my-team';
import { TeamManagementPage } from '../pages/team-management/team-management';
import { NativeAudio } from '@ionic-native/native-audio';
import { ApiProvider } from '../providers/api/api';
import { Base64} from '@ionic-native/base64';
import { Geolocation } from '@ionic-native/geolocation';
import { ViewTaskPage } from '../pages/view-task/view-task';
import { GoogleMaps, Geocoder } from '@ionic-native/google-maps';
import { Geofence } from '@ionic-native/geofence';
import { AddSitePage } from '../pages/add-site/add-site';
import { AddZonePage } from '../pages/add-zone/add-zone';
import {AgmCoreModule} from '@agm/core';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    SitesSelectionPage,
    AddtaskPage,
    AuditPage,
    TrainingPage,
    DeploymentPage,
    MeetingPage,
    GeneralPage,
    ManPowerSearchPage,
    PurchagePage,
    OthersPage,
    HistoryPage,
    PickupPage,
    DeployPopoverComponent,
    SignupPage,
    HomeScreenPage,
    ForgotPage,
    PracticePage,
    ScheduleManagementPage,
    SetUpPage,
    FindMyTeamPage,
    TeamManagementPage,
    ViewTaskPage,
    AddSitePage,
    AddZonePage
  ],
  imports: [
    BrowserModule,
    IonicImageViewerModule,
    HttpModule,
    MomentModule,
    MbscModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAzf49CfwFqb-Mv8mxvt_U46fKuAyPLfTw'
    })

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    SitesSelectionPage,
    AddtaskPage,
    AuditPage,
    TrainingPage,
    DeploymentPage,
    MeetingPage,
    GeneralPage,
    ManPowerSearchPage,
    PurchagePage,
    OthersPage,
    HistoryPage,
    PickupPage,
    DeployPopoverComponent,
    SignupPage,
    HomeScreenPage,
    ForgotPage,
    PracticePage,
    ScheduleManagementPage,
    SetUpPage,
    FindMyTeamPage,
    TeamManagementPage,
    ViewTaskPage,
    AddSitePage,
    AddZonePage
  ],
  providers: [
    StatusBar,
    MediaCapture,
    Media,
    File,
    GoogleMaps,
    Geocoder,
    Geofence,
    Geolocation,
    NativeAudio,
    Base64,
    SplashScreen,Camera,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ApiProvider,
  ]
})
export class AppModule {}
