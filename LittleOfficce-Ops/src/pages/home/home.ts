import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SitesSelectionPage } from '../sites-selection/sites-selection';
import { Storage } from '@ionic/storage'
import { SignupPage } from '../signup/signup';
import { ApiProvider } from '../../providers/api/api';
import { HomeScreenPage } from '../home-screen/home-screen';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {
  
  email: string;
  pass: string;
  user = {};
  passwordType: string = 'password';
  passwordIcon: string = 'eye-off';
  
    constructor(
      public navCtrl: NavController,
      public storage: Storage,
      public api: ApiProvider,
      ) {

  }
  ionViewDidLoad() {
  /* this.storage.get('user').then((val) => {
    this.user = val;
    if (this.user && this.user['id']) {
      this.isLoginFail = false;
      this.navCtrl.setRoot(HomePage);
    }
  }); */
}
  onClickMobileSubmit(){
    
  }

  onClickOTPSubmit(){
    this.navCtrl.push(SitesSelectionPage);
  }

  onClickError(){
    
  }

  onClickForgotPass(){
    
  }

  onLoginClick(){
    this.api.userLogin(this.email, this.pass).then(res =>{
      const data = JSON.parse(res['_body']);
      console.log(data);
    })
    this.navCtrl.push(HomeScreenPage);
  }

  onClickSignup(){
    this.navCtrl.push(SignupPage);
  }

  hideShowPassword(){
    this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
    this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }

}
