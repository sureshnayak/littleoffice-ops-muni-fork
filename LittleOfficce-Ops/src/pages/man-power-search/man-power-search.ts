import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SitesSelectionPage } from '../sites-selection/sites-selection';

/**
 * Generated class for the ManPowerSearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-man-power-search',
  templateUrl: 'man-power-search.html',
})
export class ManPowerSearchPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ManPowerSearchPage');
  }
  onClickTask(){
    this.navCtrl.push(SitesSelectionPage);
  }
}
